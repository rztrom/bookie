﻿using System.Collections.Generic;
using SQLite;
using System.Linq;

namespace Bookie
{
	public class BookkeeperManager : IBookkeeperManager
	{
		// deklarerar en instans av BookkeeperManager
		private static BookkeeperManager instance;

		// sökväg till databasen
		static string dbPath = (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal)) ;

		// referens till databasen
		private SQLiteConnection database = new SQLiteConnection(dbPath+ @"\database.db");

		// konstruktor
		private BookkeeperManager ()
		{
			// skapar upp tabeller i databasen
			database.CreateTable<Entry> ();
			database.CreateTable<TaxRate> ();
			database.CreateTable<Account> ();


			/// TODO -  bättre sätt att kolla om det går att kolla om databasen finns ? 
			if ( ExpenseAccounts.Count == 0) 
			{
				//läggs till i tabellen Account
				//pengakonton:
				database.Insert(new Account() {Number = 1910, Name = "Kassa", AccountType = "money" });
				database.Insert(new Account() {Number = 1930, Name = "Företagskonto", AccountType = "money" });
				database.Insert(new Account() {Number = 2018, Name = "Egna insättningar", AccountType = "money" });
				//inkomstkonton:
				database.Insert(new Account() {Number = 3000, Name = "Försäljning", AccountType = "income" });
				database.Insert(new Account() {Number = 3040, Name = "Försäljning av tjänster", AccountType = "income" });
				//Utgiftskonton:
				database.Insert(new Account() {Number = 5400, Name = "Förbrukningsinventarier och förbrukningsmaterial", AccountType = "expense" });
				database.Insert(new Account() {Number = 2013, Name = "Övriga egna uttag", AccountType = "expense" });
				database.Insert(new Account() {Number = 5900, Name = "Reklam och PR", AccountType = "expense" });

				// läggs till i tabellen TaxRate
				//skattesatser
				database.Insert(new TaxRate() {Rate = 0.06 });
				database.Insert(new TaxRate() {Rate = 0.15 });
				database.Insert(new TaxRate() {Rate = 0.25 });

			} 
		}
		// singelton
		public static BookkeeperManager Instance 
		{
			get
			{ 
				if (instance == null) 
				{
					instance = new BookkeeperManager();
				}
				return instance;
			}
		}
		// hämta entry med ett specifikt id
		//blev fel pos (-1) i listan när jag använde Get
		public Entry GetEntry (int id) 
		{
			return database.Table<Entry>().ElementAt(id);
		}
		// hämta konto med ett specifikt nummer
		public Account GetAccount(int numb)
		{
			return database.Get<Account> (numb);
		}
		// hämta skattevärde 
		public TaxRate GetTaxRate(int id)
		{
			return database.Get<TaxRate>(id);
		}
		#region IBookkeeperManager implementation

		// lägger till en händelse i databasen
		public void AddEntry (Entry ent)
		{
			database.Insert(ent);		
		}
		// Lista med inkomstkonton (filtrerat med typen)
		public List<Account> IncomeAccounts 
		{
			get 
			{
				return database.Table<Account>().Where(a => a.AccountType == "income").ToList();
			}
		}
		//lista med utgiftskonton (filtrerat med typen)
		public List<Account> ExpenseAccounts 
		{
			get 
			{
				return database.Table<Account>().Where(a => a.AccountType == "expense").ToList();
			}
		}
		// lista med typ av konton (filtrerat med typen)
		public List<Account> MoneyAccounts 
		{
			get 
			{
				return database.Table<Account>().Where(a => a.AccountType == "money").ToList();
			}
		}
		// lista med skattesatser
		public List<TaxRate> TaxRates 
		{
			get 
			{
				return database.Table<TaxRate>().ToList();
			}
		}
		// lista av händelser
		public List<Entry> Entries 
		{
			get 
			{
				return database.Table<Entry>().ToList();
			}
		}
		#endregion
	}
}

