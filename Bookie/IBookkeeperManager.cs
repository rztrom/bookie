using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookie
{
    public interface IBookkeeperManager
    {


		/// <summary>
        /// Gets all available IncomeAccounts in the system.
		/// </summary>
		List<Account> IncomeAccounts { get ; }

        /// <summary>
        /// Gets all available ExpenseAccounts in the system.
        /// </summary>
		List<Account> ExpenseAccounts { get ; }

        /// <summary>
        /// Gets all available MoneyAccounts in the system.
        /// </summary>
		List<Account> MoneyAccounts { get ; }

        /// <summary>
        /// Gets all available TaxRates in the system.
        /// </summary>
		List<TaxRate> TaxRates { get ; }

        /// <summary>
        /// Gets all Entries stored in the system.
        /// </summary>
		List<Entry> Entries { get ; }

        
        /// <summary>
        /// Adds a new Entry to the system
        /// </summary>
        /// <param name="e">The entry to add. Is assumed to be valid</param>
        void AddEntry(Entry e);

    }
}