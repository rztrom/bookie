﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookie
{
	[Activity (Label = "Lista av Händelser")]			
	public class EntryListActivity : Activity
	{
		private static BookkeeperManager bkManager = BookkeeperManager.Instance;
		private List<Entry> entries = bkManager.Entries;
		private ListView entryList;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// sätter aktiviteten
			SetContentView (Resource.Layout.activity_entry_list);
			// sätter rätt innehåll till listan
			entryList = FindViewById<ListView> (Resource.Id.entry_listView);
			// sätter adapter till listan
			entryList.Adapter = new EntryListAdapter(this, entries);
			// när man klickar på en rad
			entryList.ItemClick += delegate(object sender, AdapterView.ItemClickEventArgs e) {
				Intent i = new Intent(this, typeof (EntryItemDetailActivity));
				i.PutExtra("entry", e.Id.ToString());
				StartActivity(i);
			};

		}
	}
}

