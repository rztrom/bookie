﻿using SQLite;
namespace Bookie
{
	public class Entry : IEntry
	{
		public Entry ()
		{
		}

		#region IEntry implementation
		[PrimaryKey, AutoIncrement]
		// vill ej kunna sätta id:t utifrån
		public int Id { get; private set;}

		public string Date { get; set; }

		public string Account { get; set; }

		public string Type { get; set; }

		public string Description { get; set; }

		public double TotalAmount { get; set; }

		public double TaxRate { get; set; }

		public double TaxAmount 
		{
			get {return TotalAmount*TaxRate;}
		}

		public double SalesAmount 
		{
			get {return TotalAmount-TaxAmount;}
		}
		public string ImageFilePath { get; set;}

		#endregion
	}
}

