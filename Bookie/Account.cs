﻿using System;
using SQLite;

namespace Bookie
{
	public class Account : IAccount
	{
		#region IAccount implementation

		public string Name { get; set; }
		[PrimaryKey]
		public int Number { get; set; }
		public string AccountType { get; set; }
		#endregion

		public Account()
		{

		}
	}

}

