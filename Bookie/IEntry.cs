using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookie
{
    /// <summary>
    /// A transaction in the system.
    /// Can be either an income or expense transaction.
    /// Keeps track of which accounts were involved, how much money was transferred, when it took place, and how much sales tax applies to this particular transaction.
    /// </summary>
    public interface IEntry
    {
        /// <summary>
        /// The date this transaction took place.
        /// Should be on the form "YYYY-MM-DD".
        /// </summary>
        string Date { get; set; }

        /// <summary>
        /// The number of the Account associated with this transaction.
        /// This should always be a MoneyAccount (i.e. 1910, 1930, ...).
        /// </summary>
		string Account { get; set; }

        /// <summary>
        /// The number of the Account associated with this transaction's type.
        /// If this is an income transaction: denotes what kind of sale took place (services, products, ...)
        /// If this is an expense transaction: denotes what kind of purchase took place (withdrawals, advertising, etc products, ...)
        /// </summary>
		string Type { get; set; }

        /// <summary>
        /// A string describing this transaction in detail.
        /// </summary>
        string Description { get; set; }

        /// <summary>
        /// The total amount of this transaction (including sales tax)
        /// </summary>
        double TotalAmount { get; set; }

        /// <summary>
        /// The tax rate applying to this transaction (i.e. 6%, 12%, 25%, ...)
        /// </summary>
		double TaxRate { get; set; }

        /// <summary>
        /// Gets the total amount to pay in tax for this transaction.
        /// Is calculated from TaxRate and TotalAmount.
        /// </summary>
        double TaxAmount { get; }

        /// <summary>
        /// Gets the amount of this transaction (excluding sales tax).
        /// Is calculated from TotalAmount and TaxAmount.
        /// </summary>
        double SalesAmount { get; }

    }
}