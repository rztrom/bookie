﻿using System;
using SQLite;

namespace Bookie
{
	public class TaxRate
	{
		//konstruktor
		public TaxRate ()
		{
		}

		[PrimaryKey, AutoIncrement]
		// vill ej kunna sätta Id:t utifrån
		public int Id { get; private set;}
		public double Rate	{ get; set;	}
	}
}

