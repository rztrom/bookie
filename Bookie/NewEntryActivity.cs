﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Provider;
using AEnvironment = Android.OS.Environment; 
using AFile        = Java.IO.File; 
using AUri         = Android.Net.Uri; 


namespace Bookie
{
	[Activity (Label = "Lägg till Händelse")]			
	public class NewEntryActivity : Activity
	{
		private static BookkeeperManager bkManager = BookkeeperManager.Instance;

		private Button addEntryButton, picButton;
		private RadioButton incomeRb, expenseRb;
	
		private EditText dateET, descriptionET, totET;
		private Spinner typeSpinner, accountSpinner, taxSpinner;


		private Entry entry = new Entry();

		private bool hasPhoto;
		private ImageView picture;
		private AFile picDir; 
		private AFile dir; 
		private AFile myFile; 
		private AUri myUri;

		private List<Account> income, expense, money;




		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// sätter vyn
			SetContentView (Resource.Layout.activity_new_entry);

			// sätter alla element till rätt ID
			addEntryButton = FindViewById<Button> (Resource.Id.add_entry_button);
			incomeRb = FindViewById<RadioButton> (Resource.Id.income_rb);
			expenseRb = FindViewById<RadioButton> (Resource.Id.expense_rb);
			dateET = FindViewById<EditText> (Resource.Id.date_ET);
			descriptionET = FindViewById<EditText> (Resource.Id.description_ET);

			typeSpinner = FindViewById<Spinner> (Resource.Id.type_spinner);
			accountSpinner = FindViewById<Spinner> (Resource.Id.acccount_spinner);
			taxSpinner = FindViewById<Spinner> (Resource.Id.tax_spinner);

			picButton = FindViewById<Button> (Resource.Id.pic_button);

			totET = FindViewById<EditText> (Resource.Id.totamount_ET);

			picture = FindViewById<ImageView> (Resource.Id.pic_imageView);
			// hämtar bild-mappen
			picDir = AEnvironment.GetExternalStoragePublicDirectory( AEnvironment.DirectoryPictures); 
			// hämtar mappen för sparad data
			dir = new AFile(picDir, "BookkeeperData");

			// sätter default till inget foto
			hasPhoto = false;
			// sätter radiobutton till inkomst som default
			incomeRb.Checked = true;

			// sätter adaptern till rätt typ
			setTypeAdapter ();
			// sätter adaptern till konton
			setAccountAdapter();
			// sätter adaptern till skattesatsen
			setTaxAdapter ();


			// om inte filen med sökvägen finns skapar vi upp den
			if (!dir.Exists()) 
			{    
				dir.Mkdirs(); 
			}
				//klickar på "inkomst-radioButton"
			incomeRb.Click += delegate {
				setTypeAdapter();
			};

				// klickar på "utgift-radioButton"
			expenseRb.Click += delegate {
				setTypeAdapter();
			};
			 // klickar på "lägg till foto"
			picButton.Click += delegate(object sender, EventArgs e) 
			{
				// listans storlek på alla händelser
				int nr = bkManager.Entries.Count;

				// skapar ny fil som har listans storlek som nr
				myFile = new AFile(dir, "file"+nr+".jpg");

				myUri = AUri.FromFile(myFile);
				// går till kameran
				Intent i = new Intent(MediaStore.ActionImageCapture); 
				// skickar med bilden och Urin *fniss*
				i.PutExtra(MediaStore.ExtraOutput, myUri); 
				StartActivityForResult(i, 0);

			};
			 // klickar på "lägg till händelse"
			addEntryButton.Click += delegate 
			{
				// fixar så att vi kan lägga till en händelse utan bild
				try{
					entry.Date = dateET.Text;
					entry.Description = descriptionET.Text;
					// gör om strängen i edittexten till double
					entry.TotalAmount = Convert.ToDouble(totET.Text); 
					entry.TaxRate = Convert.ToDouble(taxSpinner.SelectedItem.ToString());
					entry.Type= typeSpinner.SelectedItem.ToString();
					entry.Account= accountSpinner.SelectedItem.ToString();
					if(hasPhoto){
						// sätter sökvägen för bilden till bildens namn
						entry.ImageFilePath = myFile.ToString();
						Toast.MakeText(this,"ImageFilePath : "+entry.ImageFilePath, ToastLength.Long).Show();
					}
					// lägger till händelsen i databasen
					bkManager.AddEntry(entry);
					// går tillbaka till huvudmenyn
					Intent i = new Intent(this, typeof(MainActivity));
					StartActivity(i);
				} catch {

					Toast.MakeText(this,"Du har inte fyllt i alla fält!", ToastLength.Long).Show();
				}
			};


		}


		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{    
			if (requestCode == 0 && resultCode == Result.Ok) {

				// kollar hur stor displayen är och sätter det till höjden
				int height = Resources.DisplayMetrics.HeightPixels;   
				// bildens bredd
				int width = picture.Width;  
				// gör datan till bild
				Bitmap bitmap = ImageUtils.LoadAndScaleBitmap( myUri.Path, width, height);  
				// sätter bitmappen till imageView:n
				picture.SetImageBitmap(bitmap);
				// finns ett foto 
				hasPhoto = true;
			} else {   
				base.OnActivityResult (requestCode, resultCode, data);
			}
		}

		// sätter skattetabellen till spinnern
		private void setTaxAdapter()
		{
			ArrayAdapter taxAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem ,bkManager.TaxRates.Select(a=>a.Rate).ToList());
			taxAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			taxSpinner.Adapter= taxAdapter;
		}

		// sätter rätt typ av lista till spinnern beroende på vilken radiobutton som är markerad
		private void setTypeAdapter()
		{
			if(incomeRb.Checked)
			{
				ArrayAdapter typeAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem ,bkManager.IncomeAccounts.Select(i=> i.Name +" ( "+i.Number+")").ToList());
				typeAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
				typeSpinner.Adapter= typeAdapter;
			}else {
				ArrayAdapter typeAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem ,bkManager.ExpenseAccounts.Select(i=> i.Name +" ( "+i.Number+")").ToList());
				typeAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
				typeSpinner.Adapter= typeAdapter;
			}
		}

		// sätter kontotabellen till spinnern
		private void setAccountAdapter() 
		{
			ArrayAdapter accountAdapter = new ArrayAdapter(this,Android.Resource.Layout.SimpleSpinnerItem ,bkManager.MoneyAccounts.Select(i=> i.Name +" ( "+i.Number+")").ToList());
			accountAdapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
			accountSpinner.Adapter= accountAdapter;
		}

	}
}




