using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookie
{
    /// <summary>
    /// An account with a name and a number.
    /// 
    /// Note:
    /// This is the same for all different sorts of accounts, i.e. ExpenseAccounts, IncomeAccounts and MoneyAccounts.
    /// You should differentiate between your accounts in some other way (e.g. separate lists/tables)
    /// </summary>
    public interface IAccount
    {
        /// <summary>
        /// The name of this account,
        /// E.g. Ränteintäkter
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// The number of this account,
        /// E.g. 8310
        /// </summary>
        int Number { get; set; }
    }
}