﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Bookie
{
	[Activity (Label = "Bookie", MainLauncher = true)]
	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// sätter vyn från resursen 
			SetContentView (Resource.Layout.activity_main);

			// sätter rätt id till knapparna
			Button entryButton = FindViewById<Button> (Resource.Id.new_entry_button);
			Button showEntriesButton = FindViewById<Button> (Resource.Id.show_entries_button);
			Button createReportButton = FindViewById<Button> (Resource.Id.create_report_button);


			// klickar på "lägg till"-knappen
			entryButton.Click += delegate (object sender, EventArgs e) {
				// aktiviteten vi kommer till
				Intent i = new Intent(this, typeof(NewEntryActivity));
				StartActivity (i);
			};
			//klickar på "visa händelser"-knappen
			showEntriesButton.Click += delegate(object sender, EventArgs e) {
				// aktiviteten vi kommer till
				Intent i = new Intent(this, typeof(EntryListActivity));
				StartActivity (i);

			};
			createReportButton.Click += delegate(object sender, EventArgs e) {
				// göra en rapport
			};

		}
	}
}



