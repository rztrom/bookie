﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AUri = Android.Net.Uri;
using AFile = Java.IO.File; 

namespace Bookie
{
	[Activity (Label = "Information"),]	

	public class EntryItemDetailActivity : Activity
	{
		
		private Entry entry;
		private static BookkeeperManager bkManager = BookkeeperManager.Instance;


		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// sätter layouten
			SetContentView (Resource.Layout.activity_entry_item);

			int id = Convert.ToInt32 (Intent.GetStringExtra ("entry"));
				entry = bkManager.GetEntry(id);

			// sätter textViewsen på sidan
			FindViewById<TextView> (Resource.Id.date_TV).Text = entry.Date;
			FindViewById<TextView> (Resource.Id.description_TV).Text = entry.Description;
			FindViewById<TextView> (Resource.Id.type_TV).Text = entry.Type;			
			FindViewById<TextView> (Resource.Id.account_TV).Text = entry.Account;	
			FindViewById<TextView> (Resource.Id.total_TV).Text = entry.TotalAmount.ToString();
			FindViewById<TextView> (Resource.Id.tax_TV).Text = entry.TaxRate.ToString();

			// ser till att vi klarar att gå vidare om bild saknas
			try{
				// sätter bilden om det finns nån 	
				FindViewById<ImageView> (Resource.Id.pic_IV).SetImageURI (AUri.FromFile(new AFile(entry.ImageFilePath)));
			} catch {
				Toast.MakeText(this,"Ingen bild tillagd", ToastLength.Long).Show();
			}

		}
	}
}
